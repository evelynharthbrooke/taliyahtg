#
# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

from telegram import Update
from telegram.constants import ChatAction, ParseMode


async def me(update: Update, _):
    """Retrieves information about the message author."""

    name = update.message.from_user.full_name
    username = update.message.from_user.name
    id = update.message.from_user.id

    await update.message.chat.send_action(action=ChatAction.TYPING)
    await update.message.reply_markdown_v2(f"You are *{name}* \({username}\) with the ID of `{id}`\.")


async def id(update: Update, _):
    """Retrieves the message author's user ID."""

    name = update.message.from_user.first_name
    id = update.message.from_user.id

    await update.message.chat.send_action(action=ChatAction.TYPING)
    await update.message.reply_markdown_v2(f"You are user *{name}* with ID `{id}`\.", quote=True)
