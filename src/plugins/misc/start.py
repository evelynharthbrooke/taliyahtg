#
# Copyright (C) 2016-2022 Evelyn Marie and contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

from telegram import Update
from telegram.constants import ChatAction, ParseMode
from telegram.ext import ContextTypes


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Displays a basic start message."""

    name = context.bot.first_name

    await update.message.chat.send_action(action=ChatAction.TYPING)
    await update.message.reply_markdown_v2(f"Hi, I'm *{name}*! Please enter /about to view information about me.")
